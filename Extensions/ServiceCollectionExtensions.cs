﻿namespace WebSwaggerApplication.Extensions
{
    using Microsoft.Extensions.DependencyInjection;
    using Nest;
    using WebSwaggerApplication.Config;
    using WebSwaggerApplication.Models;

    public static class ServiceCollectionExtensions
    {
        public static void AddElasticSearch(this IServiceCollection services, ElasticsearchConfig configuration)
        {
            var url = configuration.BaseUrl;
            var defaultIndex = configuration.IndexName;

            using (var settings = new ConnectionSettings(url))
            {
                settings
                    .DefaultIndex(defaultIndex)
                    .DefaultMappingFor<Car>(m => m.PropertyName(c => c.Id, "id"));

                var client = new ElasticClient(settings);
                services.AddSingleton<IElasticClient>(client);
            }
        }
    }
}
