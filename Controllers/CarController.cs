﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebSwaggerApplication.Models;
using WebSwaggerApplication.Services;

namespace WebSwaggerApplication.Controllers
{
    [ApiController]
    [Route("cars")]
    public class CarController : ControllerBase
    {
        private readonly ICarDataService carDataService;

        public CarController(ICarDataService carDataService)
        {
            this.carDataService = carDataService;
        }

        [HttpPost("index")]
        [Produces("application/json")]
        public IActionResult IndexCar([FromBody] Car car)
        {
            this.carDataService.IndexCar(car);
            return this.Ok();
        }
    }
}
