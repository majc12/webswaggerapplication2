﻿namespace WebSwaggerApplication.SearchEngine
{
    using System.Collections.Generic;
    using WebSwaggerApplication.Models;

    public interface IElasticsearchService
    {
        void InsertDocument(Car doc);

        void DeleteDocument(int id);

        List<Car> SearchByPlate(string plate);

        List<Car> SearchInFiles(string search);

        void Initialize();
    }
}
