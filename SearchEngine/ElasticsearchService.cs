﻿namespace WebSwaggerApplication.SearchEngine
{
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;
    using Nest;
    using System.Collections.Generic;
    using System.Linq;
    using WebSwaggerApplication.Config;
    using WebSwaggerApplication.Models;

    public class ElasticsearchService : IElasticsearchService
    {
        private readonly IOptions<ElasticsearchConfig> elasticConfig;
        private readonly IElasticClient elasticClient;
        private readonly ILogger<ElasticsearchService> logger;

        public ElasticsearchService(IElasticClient elasticClient, IOptions<ElasticsearchConfig> elasticConfig, ILogger<ElasticsearchService> logger)
        {
            this.elasticClient = elasticClient;
            this.elasticConfig = elasticConfig;
            this.logger = logger;
        }

        public void InsertDocument(Car doc)
        {
            var response = this.elasticClient.Index<Car>(doc, x => x.Index(this.elasticConfig.Value.IndexName));

            if (!response.IsValid)
            {
                this.logger.LogError($"Unable to create document with id {doc.Id}. Message: {response.OriginalException?.Message}");
            }
        }

        public void DeleteDocument(int id)
        {
            this.elasticClient.Delete<Car>(id);
        }

        public List<Car> SearchByPlate(string plate)
        {
            var data = this.elasticClient.Search<Car>(s => s
                .Query(q => q
                    .Match(m => m
                        .Field(f => f.Plate)
                            .Query(plate))));
            return data.Documents.ToList();
        }

        public List<Car> SearchInFiles(string search)
        {
            var results = this.elasticClient.Search<Car>(
                s => s
                    .Query(
                        q => new QueryStringQuery { Query = search, DefaultOperator = Operator.Or }));

            return results.Documents.ToList();
        }

        public void Initialize()
        {
            this.SetElasticsearchOptions();
        }

        private void SetElasticsearchOptions()
        {
            var index = this.elasticClient.Indices.Exists(this.elasticConfig.Value.IndexName);

            if (!index.Exists)
            {
                var response = this.elasticClient.Indices.Create(this.elasticConfig.Value.IndexName, c => c
                     .Settings(s => s
                         .Analysis(a => a
                             .Analyzers(ad => ad
                                 .Custom("windows_standard_analyzer", ca => ca
                                     .Tokenizer("windows_standard_tokenizer")))
                             .Tokenizers(t => t
                                 .Standard("windows_standard_tokenizer")))));

                if (!response.IsValid)
                {
                    this.logger.LogError($"Unable to create index {this.elasticConfig.Value.IndexName}. Message: {response.OriginalException.Message}");
                }
            }

            this.elasticClient.Map<Car>(c => c
                .AutoMap()
                .Properties(ps => ps
                    .Text(s => s
                        .Name(n => n.Plate)
                        .Analyzer("windows_standard_analyzer"))
                    .Number(l => l
                        .Type(NumberType.Integer)
                        .Name(n => n.Id))
                    .Number(l => l
                        .Type(NumberType.Float)
                        .Name(n => n.Kilometers))));

        }
    }
}
