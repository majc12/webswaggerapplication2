namespace WebSwaggerApplication
{
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Hosting;
    using Microsoft.Extensions.Options;
    using WebSwaggerApplication.Config;
    using WebSwaggerApplication.Extensions;
    using WebSwaggerApplication.Kafka;
    using WebSwaggerApplication.SearchEngine;
    using WebSwaggerApplication.Services;

    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.Configure<ElasticsearchConfig>(Configuration.GetSection("ElasticSearchOptions"));
            services.Configure<KafkaConfig>(Configuration.GetSection("KafkaOptions"));
#pragma warning disable ASP0000
            var elasticSearch = services.BuildServiceProvider().GetService<IOptions<ElasticsearchConfig>>().Value;
            services.AddElasticSearch(elasticSearch);

            services.AddSwaggerGen();

            this.InstallServices(services);
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();
            app.UseRouting();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
            
            app.UseSwagger(c =>
            {
                c.SerializeAsV2 = true;
            });

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),  
            // specifying the Swagger JSON endpoint.  
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
                c.RoutePrefix = string.Empty;
            });
        }

        private void InstallServices(IServiceCollection services)
        {
            services.AddSingleton<IElasticsearchService, ElasticsearchService>();
            services.AddSingleton<IKafkaMessageProcessor, KafkaMessageProcessor>();
            services.AddSingleton<ICarDataService, CarDataService>();
        }
    }
}
