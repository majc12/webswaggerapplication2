﻿namespace WebSwaggerApplication.Services
{
    using WebSwaggerApplication.Kafka;
    using WebSwaggerApplication.Models;
    using WebSwaggerApplication.SearchEngine;

    public class CarDataService : ICarDataService
    {
        private const string Topic = "gft.car";
        private readonly IKafkaMessageProcessor kafkaMessageProcessor;
        private readonly IElasticsearchService elasticsearchService;

        public CarDataService(IKafkaMessageProcessor kafkaMessageProcessor, IElasticsearchService elasticsearchService)
        {
            this.kafkaMessageProcessor = kafkaMessageProcessor;
            this.elasticsearchService = elasticsearchService;

            this.elasticsearchService.Initialize();
        }

        public void IndexCar(Car car)
        {
            this.elasticsearchService.InsertDocument(car);
            this.kafkaMessageProcessor.ProduceMessage(car, Topic);
        }
    }
}
