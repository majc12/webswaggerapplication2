﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebSwaggerApplication.Models;

namespace WebSwaggerApplication.Services
{
    public interface ICarDataService
    {
        void IndexCar(Car car);
    }
}
