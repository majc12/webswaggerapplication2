﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebSwaggerApplication.Config
{
    public class ElasticsearchConfig
    {
        public Uri BaseUrl { get; set; }
        public string IndexName { get; set; }
    }
}
