﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebSwaggerApplication.Config
{
    public class KafkaConfig
    {
        public string BrokerList { get; set; }
    }
}
