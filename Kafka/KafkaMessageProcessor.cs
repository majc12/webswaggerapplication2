﻿namespace WebSwaggerApplication.Kafka
{
    using Confluent.Kafka;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;
    using Newtonsoft.Json;
    using System;
    using WebSwaggerApplication.Config;

    public class KafkaMessageProcessor : IKafkaMessageProcessor
    {
        private readonly IOptions<KafkaConfig> config;
        private readonly ILogger<KafkaMessageProcessor> logger;

        public KafkaMessageProcessor(IOptions<KafkaConfig> config, ILogger<KafkaMessageProcessor> logger)
        {
            this.config = config;
            this.logger = logger;
        }

        public void ProduceMessage(object data, string topic)
        {
            var producerConfig = new ProducerConfig { BootstrapServers = this.config.Value.BrokerList };
            
            using (var producer = new ProducerBuilder<string, string>(producerConfig).Build())
            {
                try
                {
                    var message = JsonConvert.SerializeObject(data);
                    var result = producer.ProduceAsync(topic, new Message<string, string> { Key = Guid.NewGuid().ToString(), Value = message }).Result;
                }
                catch (Exception e)
                {
                    this.logger.LogError(e, $"Error trying to produce a message on topic {topic}.");
                }
            }
        }
    }
}
