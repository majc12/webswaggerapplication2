﻿namespace WebSwaggerApplication.Kafka
{
    public interface IKafkaMessageProcessor
    {
        void ProduceMessage(object data, string topic);
    }
}
